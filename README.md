# Initiation React

React est le framework JavaScript le plus utilisé pour la création d'interfaces web.
Nous allons créer une page web interactive inspirée de TripAdvisor, où nous pourrons consulter des pages d'hôtels ou de restaurants.

## NodeJS
Vous aurez besoin de NodeJS, que vous pouvez télécharger [ici](https://nodejs.org/en/) (préférez la version LTS).

Pour la suite du TP, vérifiez que vous avez bien accès à la commande `npm`, qui permet d'utiliser le gestionnaire de package de Node.

## Create React App
Pour créer une nouvelle application React, nous allons utiliser `Create React App`. Il configure à notre place l'environnement de développement et optimise l'application pour la production.

Une fois l'installation terminée, exécutez la commande suivante :
```bash
npx create-react-app nom-app
```

Elle créera un dossier portant le nom de votre application, avec de nombreuses configurations et utilitaires utile au développement de l'application.

### Structure du projet

#### `package.json`
Le fichier `package.json` organise votre projet. Il contiendra la liste de vos dépendances, des scripts qui encapsulent les commandes importantes pour le projet et plus encore. 

#### `App.js`
C'est le point d'entrée de votre application, c'est le composant racine (on y reviendra).

#### `App.test.js`
Create React App embarque un framework de test unitaire pour tester vos composants. Ce fichier contient un test unitaire du composant `App` créé par défaut.

### `public/`
Ce dossier contiendra tous les assets que vous allez servir sur le web, comme l'icône du site, ou le fichier `index.html`.

## Développement

Vous pouvez utiliser la commande `npm run start` pour lancer le serveur en développement et voir la page par défaut créée par Create React App sur [localhost:3000](https://localhost:3000). 

Remarquez que quand vous modifiez quelque chose dans le code source, la page se met à jour automatiquement.

## Composants

Rentrons dans le vif du sujet. React est basé sur le concept de composants, qui consistent à diviser votre interface utilisateur en éléments indépendants et réutilisables.

React utilise le langage appelé JSX. C'est un langage qui permet d'allier l'html avec le JavaScript. Ce code JSX sera compilé par des outils comme Babel pour produire du code JavaScript qui pourra être supporté par tous les navigateurs, y compris les plus anciens. Nous ne rentrons pas dans les détails dans ce cours. 

### Classes vs fonctions

Avant la version 16.8 de React, on définissait un composant en spécialisant la classe `Component`, en re-définissant la méthode render, qui renvoie l'aspect graphique du composant :
```jsx
import React from "react";

class MyComponent extends React.Component {
  render() {
    return (
      <div>
        Hello, {this.props.name}
      </div>
    );
  }
}
```

Il est maintenant possible d'utiliser des fonctions, de façon à simplifier la syntaxe :
```jsx
import React from "react";

function MyComponent(props) {
  return (
    <div>
      Hello, {props.name}
    </div>
  );
}
```

Les deux composants ci-dessus sont équivalents.

### Props
On remarquera dans les deux extraits de code précédants la notion de props. Ce sont les données d'entrée de votre composant.
Par exemple, on pourra utiliser le composant `MyComponent` de cette façon :
```jsx
<MyComponent name="world" />
```
Le rendu sera `<div>Hello, world</div>`.

## Hooks
Les hooks sont des fonctions spéciales pour les composants fonctionnels de React. Il en existe une dizaine, mais 2 d'entre eux sont primordiaux.

### useState
Un composant peut contenir un "état local", une donnée qui est similaire aux props mais qui est totalement contrôlée par le composant.

On définit l'état local d'un composant grâce au hook d'état `useState` : 

```jsx
function Counter() {
  const [count, setCount] = useState(0);

  return (
    <button onClick={() => setCount(count + 1)}>
      Cliquez pour incrémenter : {count}
    </button>
  );
}
```

Notons plusieurs choses : 

Premièrement, `useState` est une fonction qui prend un paramètre qui la valeur initiale de l'état. Elle renvoie un tableau de 2 valeurs, que l'on peut récupérer directement à l'aide du _destructuring_.
Le premier élément retourné est la valeur courante de l'état, on peut l'afficher dans du JSX avec des crochets. 
La deuxième valeur retournée est une fonction, qui permet de modifier l'état et de raffraichir le rendu du composant à l'utilisateur.

Il est impossible de modifier l'état via une affectation (si `count` était défini avec un `let` puis en faisant `count = 3` par exemple), car cela ne raffraichirait pas l'affichage du composant.

Enfin, on capture l'évènement du clic sur un bouton avec la prop `onClick` de l'élément `button`. On utilise une fonction fléchée (une lambda ou une fonction anonyme) pour appeler `setCount`.

### useEffect
On voudra souvent exécuter du code au chargement d'un composant, par exemple pour aller chercher des données sur un serveur distant via une requête HTTP, ou au contraire pour libérer des ressources quand le composant est détruit.
On utilisera le hook `useEffect` pour gérer le cycle de vie du composant.

L'utilisation de `useEffect` peut paraître assez barbare :
```jsx
function Counter() {
  const [count, setCount] = useState(0);

  useEffect(() => {
    console.log(`Count = ${count}`);
  });

  return (
    <button onClick={() => setCount(count + 1)}>
      Cliquez pour incrémenter : {count}
    </button>
  );
}
```

La fonction mise en paramètre du hook sera exécutée à chaque fois que le composant se met à jour (via une modification d'une prop ou d'un état). 
Si on veut l'exécuter lorsque la valeur d'une prop ou d'un état particulier est modifiée, on pourra passer un tableau des dépendances dans le deuxième argument de `useEffect` :

```jsx
function Counter() {
  const [count, setCount] = useState(0);

  useEffect(() => {
    console.log(`Count = ${count}`);
  }, [count]);

  return (
    <button onClick={() => setCount(count + 1)}>
      Cliquez pour incrémenter : {count}
    </button>
  );
}
```

De cette façon, la fonction est exécutée seulement quand la valeur de `count` change après un re-render du composant.

Lorsque l'on veut simplement exécuter une fonction au chargement d'un composant, on laisse la liste des dépendances vide, par exemple :
```jsx
function Users() {
  const [users, setUsers] = useState([]);
  
  useEffect(() => {
    fetch('/users')
      .then(response => response.json())
      .then(json => setUsers(json));
  }, []);

  return (
    <>
      {users.map(user =>
        <User name={user} key={user} />
      )}
    </>
  );
}
```
On remarquera sur cet exemple l'utilisation des fragments (les balises vides `<>` et `</>`) qui permettent de mettre plusieurs éléments au même niveau sans ajouter un noeud parent au DOM.

On pourra exécuter une fonction à la destruction du composant en renvoyant une fonction de nettoyage de cette façon :
```jsx
useEffect(() => {
  const timer = setInterval(() => console.log('test'), 2000);

  return () => clearInterval(timer);
}, []);
```

### useContext

Le contexte permet de faire transiter un état global sur toute une hiérarchie de composants. Ils peuvent être utilisés pour définir un style d'affichage partiuclier pour toute l'application, ou afin d'optimiser le nombre d'appels exécutés à une API externe.

#### Première étape : Créer un contexte

On utilisera la fonction ``createContext`` pour créer un objet contexte. On passe en argument de la fonction la valeur par défaut du contexte.
```js
import { createContext } from 'react';

const ColorModeContext = createContext({
  mode: '',
  toggleColorMode: () => {}
});
```

Le composant au sommet de la hiérarchie va fournir le contexte à ses composants enfant, en partageant un état à l'aide du composant ``Provider`` rattaché au contexte.

```js
function ColorModeProvider({ children }) {
  const [mode, setMode] = useState('light');

  const toggleColorMode = () => {
    setMode(mode === 'light' ? 'dark' : 'light');   
  }

  return (
    <ColorModeContext.Provider value={{ mode, toggleColorMode }}>
      {children}
    </ColorModeContext.Provider>
  )
}
```
Il faut noter l'utilisation de la prop implicite ``children`` présente sur tous les composants, qui permet d'obtenir tous les enfants du composant.

Un composant enfant pourra utiliser le contexte avec le hook ``useContext``.
```js
function ToggleModeButton() {
  const { mode, toggleColorMode } = useContext(ColorModeContext);

  return (
    <button onClick={toggleColorMode}>
      {mode === 'light' ? <DarkIcon /> : <LightIcon />}
    </button>
  )
}
```

On utilisera le composant pour modifier le mode dans les fils du provider.
```js
function App() {
  return (
    <ColorModeProvider>
      <Header>
        <Title />
        <ToggleModeButton />
      </Header>
      <Content />
    </ColorModeProvider>
  )
}
```
Tous les composants de la hiérarchie comme  ``Content``, ``Header`` pourront obtenir la valeur du contexte pour se styliser de façon dynamique.

```js
function Header({ children }) {
  const { mode } = useContext(ColorModeContext);

  return (
    <div
      style={{
        backgroundColor: mode === 'light' ? '#000' : '#FFF'
      }}
    >
      ...
    </div>
  )
}
```

### Affichage conditionnel et maps
Cette syntaxe très flexible qui mélange JavaScript et HTML vous permet par exemple de retourner un certain composant en fonction d'une condition, par exemple : 
```jsx
function Dashboard({ name, role }) {
  if (role === "admin") {
    return <AdminDashboard />
  }
  return <UserDashboard />
}
```

D'une même façon, vous pouvez utiliser les `map` du JavaScript pour traiter plusieurs composants en même temps, par exemple, avec une liste d'utilisateurs : 
```jsx
function Dashboard({ users }) {
  return (
    <>
      {users && users.map(user => 
        <User name={user} key={name} />
      )}
    </>
  );
}

```
Il faut noter que lorsque l'on utilise des listes de cette façon, il est nécessaire de spécifier la prop `key` dans les composants enfants.

### Remonter des données aux composants parents
Avec React, vous serez souvent amenés à vouloir faire remonter un état d'un composant enfant vers un composant parent. Pour ce faire, il faut passer une composant dans les props du composant enfant, que l'on appelera dans ce dernier. 

```jsx
function User({ name, onDelete }) {
  return (
    <>
      <h3>{name}</h3>
      <button onClick={() => onDelete(name)}>
        Supprimer
      </button>
    </>
  );
}

function Users() {
  const [users, setUsers] = useState([]);
  
  useEffect(() => {
    fetch('/users')
      .then(result => result.json())
      .then(json => setUsers(json));
  }, []);

  const handleDelete = (name) => {
    // Copie des utilisateurs sans celui que l'on supprime
    const filteredUsers = users.filter((user) => user.name !== name);
    setUsers(usersCopy);
  }

  return (
    <>
      {users && users.map(user =>
        <User 
          key={user}
          name={user}
          onDelete={handleDelete}
        />
      )}
    </>
  );
}
```

## Aller plus loin & écosystème React
* [Documentation React](https://reactjs.org/)
* [Awesome React (Compendium de l'écosystème React)](https://github.com/enaqx/awesome-react)
* [NextJS (Rendu côté serveur)](https://nextjs.org/)
* [Gatsby](https://github.com/gatsbyjs/gatsby)
* [Create React App](https://create-react-app.dev/)
* [React DevTools (Extension de navigateur pour debugger)](https://github.com/facebook/react-devtools)
* [React Native (Développement mobile multi-plateformes)](https://reactnative.dev/)

### Bibliothèques de composants
* [React Bootstrap](https://react-bootstrap.github.io/)
* [Material UI (Material Design pour React)](https://mui.com/)
* [Ant Design](https://ant.design/)
* [Chakra UI](https://chakra-ui.com/)

### Autres bibliothèques utiles 
* [Redux (Gestion de l'état)](https://redux.js.org/)
* [React Router (Routage côté client)](https://reactrouter.com/web/guides/quick-start)
* [React Spring (Animations)](https://react-spring.io/)
